# MOOC FLOSS

## Hi and Welcome! 

This repository holds what will soon become a <abbr title="Massive Open Online Course">MOOC</abbr> about contributing to Free, Libre, and Open Source Software (FLOSS). However, because everyone loves a bit of meta, this MOOC sees itself as a Free, Libre, and open source project, published with a CC-BY-SA license! We'll also make sure to use only FLOSS tools to make the content of this MOOC.

This license and organization will allow to respect the principles outlined in the [UNESCO Open Educational Resources](https://unesdoc.unesco.org/ark:/48223/pf0000157987.locale=fr) framework ([Guidelines](https://unesdoc.unesco.org/ark:/48223/pf0000213605) ), aiming to produce "materials used to support education that may be freely accessed, reused, modified, and shared" (UNESCO words, not mine). If you are familiar with the Free software definitions, you might notice some similarity.


As a first version, this repository is roughly organized as follows:

```
.
├── README.md
├── scenarisation
│   └── README.md
├── W1-What
│   ├── README.md
│   ├── assets
│   │   └── logo.svg
│   ├── brainstorm.md
│   └── pages
├── W2-Where
…
```

Tentative logic for organizing stuff:

 - Each folder should have a README.md file, describing its contents.

 - The global organization of the MOOC is described in the [scenarisation/](scenarisation/README.md) folder.

 - Each week should contain its own assets.

 - The `pages/` folder will contain the actual content of the MOOC, that will
   be visible to the students on the platform. The plan is to write it in
   Markdown, then generate a page with e.g. Hugo in the CI and embed (as an
   iframe) the corresponding gitlab.io page in the MOOC platform.

 - Feel free to open an issue or a <abbr title="Merge Request" >MR</abbr> for all questions or suggestions ☺ 

 - There is no such thing as "Too many references" 



## Team and sponsors

> This section **needs expansion**. You can help by [adding to it](https://gitlab.com/mooc-floss/mooc-floss/-/merge_requests)

 * Marc Jeanmougin is a research engineer at [Télécom Paris](https://www.telecom-paris.fr) and a contributor to free software such as Inkscape.
 * Rémi Sharrock is a professor at [Télécom Paris](https://www.telecom-paris.fr)
 * [Framasoft](https://framasoft.org) is a popular education nonprofit in
   France, focused on promotion, dissemination and development of free software.



