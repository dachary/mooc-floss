# Personas

These personas represent six types of people that could be interested in
followig this MOOC. It is important, when designing the courses, to ask
yourself how any of them will be able to follow the module, or of there is any
predictable pain point that could arise. It is not necessary that all of them
should be able to understand, follow entirely, or pass, the course.



## Anya, 30, Moscow (Russia)
Unemployed, Computer+++, Internet+++
### Description
She left her previous IT job after having a daughter, wants a remote job in IT,
knows about the existence of FLOSS, and wants to see if it's possible to get a job in this area
### Motivations
Find a job / Find contacts
### Frustrations
Needs to work at her own pace
Lots of potential distractions at unpredictable times
### Skills
Coding ++
English ++
### Needs
Understanding the economic stakes and structures of FLOSS projects

## Paawanjeet, 19, Bombay (India)
Student, Computer+, Internet-
### Description
Bachelor program in computer engineering, not necessarily fluent in English.
Internet access is 4G or Internet cafe, or university
### Motivations
Wants to participate in GSoC
### Frustrations
Might not understand what everything is about, might get lost.
May not have all the technical prerequisites for the course
### Skills
Basic programming
### Needs
Be guided to engage with FLOSS communities

## Mariam, 26, Tunis (Tunisia)
PhD candidate, Computer++, Internet+++
### Description
Does a PhD in Machine learning. Colorblind.
### Motivations
Wants to apply here theoretical research results into reference open source
software (such as scikit)
### Frustrations
Academic papers are too often left as prototypes, withtout community, sometimes
without opportunities to efficiently network
### Skills
Well versed in programming prototypes programs. At the forefreont of knowledge
in her area of research.
### Needs
How to get into existing, widely used, open source software, how to communicate
with them, and how to make her work used by potentially millions of people

## Eliza, 43, Sydney (Australia)
Dev for a GAFAM, Computer--, Internet++
### Description
Uses FLOSS tools at work (e.g. VSCode) and knows about it, wants to learn about
it during commute time (not a lot of time out of that)
### Motivations
Wants to gain competence on collaborations or on a new programming language and
understand how FLOSS communities work. Ideally, would want to apply those
processes and ideas in her work team. 
### Frustrations
Mostly knows the tools used internally in her work (specific libraries or code
forges), LACK OF TIME, hard to concentrate in public transports
### Skills
Github++, Coding++, teamwork++
### Needs
Might specifically need summaries of previous contents to get back quickly in
the course after a day of work

## Seth, 38, Salem (OR, USA)
IT Contractor, Computer+++, Internet++
### Description
Uses, sells, hosts, and recommends free software to his clients. His experience and
feedback from clients can help those software, and he can provide more value to
his clients by providing better software, so he wants to help those. Very
social, wants to engage on the forums.
### Motivations
Better products, happier clients.
### Frustrations
Already looked at code from big projects, finds it very complicated (too much
code!), does not know where to learn how to engage or code for big projects
### Skills
Programming, Sales, Administration
### Needs
to improve a free software, and to know about potential license or other legal problems

## Thomas, 55, Rennes (France)
CEO of a small company
### Description
Sells specific proprietary software for an industry, but has employees who are
fan of FLOSS. He's worried about Intellectual Property matters and curious to
see how FLOSS works in practice, but will hesitate to follow the course as his
main motivation is not to contribute.
### Motivations
As adversized by his employees, using "Open Source" to increase his company's
visibility or client base.
### Frustrations
Worried about his company strategy if people steal his work or ideas; language
barrier (mostly speaks french)
### Skills
Code, admin, marketing
### Needs
Needs to understand if it can be useful of be dangerous for him, + how to "do
it well". More interested by the more theoretical content.



